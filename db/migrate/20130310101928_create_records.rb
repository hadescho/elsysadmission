class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.boolean :medical_note
      t.boolean :diploma

      t.timestamps
    end
  end
end
