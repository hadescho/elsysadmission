class Record < ActiveRecord::Base
#  require 'csv'

  attr_accessible :diploma, :first_name, :last_name, :medical_note, :middle_name

   def self.to_csv (options = {})
   	CSV.generate(options)  do |csv|
   		csv << column_names
   		all.each do |record|
   			csv<< record.attributes.values
   		end
   	end
   end


end
