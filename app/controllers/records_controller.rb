class RecordsController < ApplicationController
  
  def new
    @record = Record.new
  end

  def create
    @record = Record.new
    @record.first_name   = params[:record][:first_name]
    @record.middle_name  = params[:record][:middle_name]
    @record.last_name    = params[:record][:last_name]
    @record.medical_note = params[:record][:medical_note]
    @record.diploma      = params[:record][:diploma]
    @record.save
    redirect_to :root
  end

  def show
  end

  def index
    @records = Record.all
    respond_to do |format|
      format.html
      format.csv {send_data  Record.to_csv}
      format.xls
    end
  end

  def edit
  end

  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    redirect_to :root
  end
end
